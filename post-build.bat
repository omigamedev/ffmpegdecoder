@echo off
set SOLUTION=%1
set DEST=%2
set PLATFORM=%3
set ARCH=%4

xcopy Shaders %DEST%"\Shaders\" /D /y
xcopy "Libs\glew-1.13.0\bin\Release\%PLATFORM%\glew32.dll" %DEST% /D /y
xcopy "Libs\glfw-3.1.2.bin.WIN%ARCH%\lib-vc2013\glfw3.dll" %DEST% /D /y

cd "Libs\ffmpeg-2.8-win%ARCH%-dev\bin"
for /r %%f in (*.dll) do @xcopy "%%f" %2 /D /y

rem cd "Libs\libav\win%ARCH%\usr\bin"
rem for /r %%f in (*.dll) do @xcopy "%%f" %2 /D /y

cd %SOLUTION%
