#version 120
uniform mat4 proj;
uniform mat4 modelview;
uniform mat4 texmat;

void main()
{
    gl_Position = proj * modelview * gl_Vertex;
    vec4 coord = texmat * gl_MultiTexCoord0;
    float y = ((1.0 - coord.y) / 3.0) * 2.0;
    float y2 = - coord.y / 3.0;
    gl_TexCoord[0] = vec4(coord.x, y, 0.0, 1.0);
    gl_TexCoord[1] = vec4(coord.x * 0.5, y2 , 0.0, 1.0);
    gl_TexCoord[2] = vec4(coord.x * 0.5 + 0.5, y2 , 0.0, 1.0);
}
