#pragma once

#define GLM_FORCE_RADIANS
#define GLFW_EXPOSE_NATIVE_WIN32
#define GLFW_EXPOSE_NATIVE_WGL
#define _USE_MATH_DEFINES

//#define USE_LIBAV
#define USE_FFMPEG

#include "targetver.h"
#include <windows.h>
#include <stdio.h>
#include <tchar.h>

#include <condition_variable>
#include <algorithm>
#include <cassert>
#include <chrono>
#include <memory>
#include <vector>
#include <thread>
#include <mutex>
#include <deque>
#include <ratio>
#include <map>

#include <al.h>
#include <alc.h>

#include <GL\glew.h>
#include <GL\wglew.h>
#include <GLFW\glfw3.h>
#include <GLFW\glfw3native.h>

#include <glm\glm.hpp>
#include <glm\ext.hpp>
#include <glm/gtc/matrix_transform.hpp>

//#ifndef _DEBUG
//#define printf(...) ((void)0)
//#endif
