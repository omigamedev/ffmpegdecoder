#include "stdafx.h"
#include "GL\Shader.h"
#include "GL\Primitive.h"
#include "GL\ShaderManager.h"
#include "GL\RenderTexture.h"
#include "Decoder.h"

class BasePlayer
{
	using This = BasePlayer;
	Format file;
	Timer timer;
	ALCdevice*  oalDevice = nullptr;
	ALCcontext* oalContext = nullptr;
	ALuint audio_source = 0;
	GLuint video_tex = 0;
	int video_w = 0;
	int video_h = 0;
	std::deque<ALuint> audio_buffer_queue;
	std::thread vthread;
	std::thread athread;
	volatile bool running = false;
	volatile bool playing = false;
	std::condition_variable play_cv;
	std::mutex mutex;
	HGLRC glrc;
	HDC dc;
public:
	template<typename FrameType>
	bool Sync(const FrameType& frame)
	{
		if (!frame.valid) return false; //todo: how about using an enum??

		// Sync with PTS
		auto dt = frame.pts - timer.Get(); //todo: limit max to frame period
		while (running && dt >= 0)
		{
			if (!playing)
			{
				// Wait for the play signal
				std::unique_lock<std::mutex> lock(mutex);
				play_cv.wait(lock, [&]{ return !running | playing; });
			}
			std::this_thread::sleep_for(std::chrono::milliseconds((int)(dt * 1000)));
			dt = frame.pts - timer.Get();
		}
		return true;
	}
	void VideoThread()
	{
		wglMakeCurrent(dc, glrc);
		int firstFrame = 2;
		while (running)
		{
			auto frame = file.videoStreams[0].GetFrame(timer.Get());
			if (firstFrame <= 0)
			{
				if (!Sync(frame))
					break;
			}
			else
			{
				firstFrame--;
			}

			// Upload decoded frame to texture data
			glBindTexture(GL_TEXTURE_2D, video_tex);
			glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, video_w, video_h + video_h / 2, GL_LUMINANCE, GL_UNSIGNED_BYTE, frame.data);

			printf("BasePlayer::VideoThread - render frame time: %fs delay: %fs\r", frame.pts, timer.Get() - frame.pts);
		}
		printf("BasePlayer::VideoThread - END\n");
	}
	void AudioThread()
	{
		ALint val;
		while (running)
		{
			auto frame = file.audioStreams[0].GetFrame();
			if (!Sync(frame))
				break;
			//printf("   BasePlayer::AudioThread - render frame\n");

			ALuint albuffer;
			alGenBuffers(1, &albuffer);
			alBufferData(albuffer, AL_FORMAT_STEREO16, frame.data, frame.size, frame.sampleRate);
			alSourceQueueBuffers(audio_source, 1, &albuffer);
			audio_buffer_queue.push_front(albuffer);
			CleanAudioBuffer();
			alGetSourcei(audio_source, AL_SOURCE_STATE, &val);
			if (playing && val != AL_PLAYING)
				alSourcePlay(audio_source);
		}
		printf("BasePlayer::AudioThread - END\n");
	}
	GLuint CreateTexture(int width, int height)
	{
		GLuint tex;
		glGenTextures(1, &tex);
		glBindTexture(GL_TEXTURE_2D, tex);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_LUMINANCE, width, height + height / 2,
			0, GL_LUMINANCE, GL_UNSIGNED_BYTE, nullptr);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glBindTexture(GL_TEXTURE_2D, 0);
		return tex;
	}
	// Not thread-safe
	void CleanAudioBuffer()
	{
		ALint val;
		// Remove consumed buffers
		alGetSourcei(audio_source, AL_BUFFERS_PROCESSED, &val);
		while (val--)
		{
			alSourceUnqueueBuffers(audio_source, 1, &audio_buffer_queue.back());
			alDeleteBuffers(1, &audio_buffer_queue.back());
			audio_buffer_queue.pop_back();
		}
	}
	bool Init(HWND hWnd, HGLRC parent)
	{
		dc = GetDC(hWnd);
		glrc = wglCreateContext(dc);
		wglShareLists(parent, glrc);
		av_register_all();
		oalDevice = alcOpenDevice(nullptr);
		oalContext = alcCreateContext(oalDevice, nullptr);
		alcMakeContextCurrent(oalContext);
		alGenSources(1, &audio_source);
		return true;
	}
	void Destroy()
	{
		if (audio_source)
		{
			CleanAudioBuffer();
			alDeleteSources(1, &audio_source);
		}
		alcMakeContextCurrent(nullptr);
		alcDestroyContext(oalContext);
		alcCloseDevice(oalDevice);
	}
	bool Open(const std::string filename)
	{
		if (!file.Open(filename.c_str()))
			return false;
		video_w = file.videoStreams[0].width;
		video_h = file.videoStreams[0].height;
		video_tex = CreateTexture(video_w, video_h);
		running = true;
		vthread = std::thread(&This::VideoThread, this);
		athread = std::thread(&This::AudioThread, this);
		return true;
	}
	void Start()
	{
		alSourcePlay(audio_source);
		timer.Start();
		playing = true;
		play_cv.notify_all();
	}
	void Pause()
	{
		alSourcePause(audio_source);
		timer.Stop();
		playing = false;
	}
	void Close()
	{
		alSourceStop(audio_source);
		running = false;
		playing = false;
		timer.Stop();
		file.Stop();
		play_cv.notify_all();
		if (vthread.joinable()) vthread.join();
		if (athread.joinable()) athread.join();
	}
	void Toggle()
	{
		printf("BasePlayer::Toggle\n");
		playing ? Pause() : Start();
	}
	GLuint GetTextureID() { return video_tex; }
};

//*******************************
//*********** MAIN CODE *********
//*******************************


BasePlayer player;
GLFWwindow* window;
Plane plane;
ShaderManager sm;

void window_key(GLFWwindow* w, int key, int scancode, int action, int mods)
{
	if (action == GLFW_PRESS)
	{
		switch (key)
		{
		case GLFW_KEY_ESCAPE:
			glfwSetWindowShouldClose(w, true);
			break;
		case GLFW_KEY_SPACE:
			player.Toggle();
			break;
		case GLFW_KEY_Q:
			player.Close();
			break;
		}
	}
}
void window_size(GLFWwindow* window, int w, int h)
{
	glViewport(0, 0, w, h);
}

int main(int argc, char* argv[])
{
	glfwInit();
	window = glfwCreateWindow(800, 600, "Viewer", nullptr, nullptr);
	glfwSetKeyCallback(window, window_key);
	glfwSetWindowSizeCallback(window, window_size);
	glfwShowWindow(window);
	glfwMakeContextCurrent(window);
	glewInit();

	printf("Graphic Vendor  : %s\n", glGetString(GL_VENDOR));
	printf("Graphic Renderer: %s\n", glGetString(GL_RENDERER));
	printf("GL  : %s\n", glGetString(GL_VERSION));
	printf("GLSL: %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));
	printf("GLFW: %s\n", glfwGetVersionString());
	printf("GLEW: %s\n", glewGetString(GLEW_VERSION));
	printf("AL  : %s\n", alGetString(AL_VERSION));
	printf("Audio Renderer: %s\n", alGetString(AL_RENDERER));

	// Init OpenGL state
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_DEPTH_TEST);
	glEnableClientState(GL_VERTEX_ARRAY);
	//glEnableClientState(GL_NORMAL_ARRAY);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);
	glViewport(0, 0, 800, 600);
	glClearColor(.3f, .3f, .3f, 1.f);

	auto proj = glm::mat4();// glm::perspective(glm::radians(45.f), 800.f / 600.f, 0.1f, 100.f);
	auto orth = glm::ortho(-1.f, 1.f, -1.f, 1.f);
	auto modelview = glm::translate(glm::vec3(0, 0, 0));
	
	plane.create(2, 2);
	sm.loadShader("yuv420");
	sm.loadShader("texture-flat");
	sm.loadShader("color-flat");

	//sm.useShader("color-flat");
	//sm.uniformMatrix4f("proj", proj);
	//sm.uniformMatrix4f("modelview", modelview);
	//sm.uniform4f("color", glm::vec4(1, 1, 0, 1));

	sm.useShader("yuv420");
	sm.uniformMatrix4f("texmat", glm::mat4());
	sm.uniformMatrix4f("proj", orth);
	sm.uniformMatrix4f("modelview", modelview);
	sm.uniform1i("tex0", TEX_DIFFUSE);
	
	player.Init(glfwGetWin32Window(window), glfwGetWGLContext(window));
	player.Open(argv[1]);
	plane.material.diffuse_tex = player.GetTextureID();
	while (!glfwWindowShouldClose(window))
	{
		//glfwWaitEvents();
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		plane.draw(GL_TRIANGLES);
		glfwSwapBuffers(window);
		glfwPollEvents();
	}
	player.Close();
	player.Destroy();

	glfwDestroyWindow(window);
	glfwTerminate();
	//system("pause");
	return 0;
}
