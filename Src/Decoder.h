#pragma once

extern "C"
{
#if defined(USE_FFMPEG)
#include <libswresample\swresample.h>
#elif defined(USE_LIBAV)
#include <libavresample\avresample.h>
#endif
#include <libavformat\avformat.h>
#include <libavcodec\avcodec.h>
#include <libavutil\avutil.h>
#include <libavutil\opt.h>
}

#pragma comment (lib, "avcodec.lib")
#pragma comment (lib, "avformat.lib")
#pragma comment (lib, "avutil.lib")
#if defined(USE_FFMPEG)
#pragma comment (lib, "swresample.lib")
#elif defined(USE_LIBAV)
#pragma comment (lib, "avresample.lib")
#endif

const char* error2str(int err)
{
	//static char str[AV_ERROR_MAX_STRING_SIZE]{0};
	static char str[256]{0};
	if (av_strerror(err, str, sizeof(str)) < 0)
		return "Unknown error";
	return str;
}

struct Packet
{
private:
	AVPacket pkt{};
	bool valid = false;
	Packet(const Packet&) = delete;
	Packet& operator=(const Packet&) = delete;
public:
	Packet() { av_init_packet(&pkt); }
	Packet(const AVPacket& pkt) : pkt(pkt) { valid = true; }
	Packet(Packet&& o) : pkt(o.pkt), valid(o.valid) { av_init_packet(&o.pkt); }
	Packet& operator=(Packet&& o) { Packet(std::forward<Packet>(o)); }
	~Packet() { av_free_packet(&pkt); }
	AVPacket Get() { return pkt; }
	bool Valid() const { return valid; }
};

class Timer
{
	std::chrono::system_clock::time_point start;
	volatile bool running;
	double elapsed{ 0 };
	double _GetElapsed()
	{
		if (running)
		{
			auto stop = std::chrono::steady_clock::now();
			auto dt = std::chrono::duration<float>(stop - start).count();
			start = stop;
			elapsed += dt * speed;
		}
		return elapsed;
	}
public:
	double speed{ 1.0 };
	static Timer& Global() { static Timer singleton;  return singleton; }
	void Start() { if (running) return; running = true; start = std::chrono::steady_clock::now(); }
	void Stop() { if (!running) return; elapsed = Get(); running = false; }
	void Toggle() { running ? Stop() : Start(); }
	void Reset() { elapsed = 0; }
	double Get() { return _GetElapsed(); }
};

template<typename T, int Max = 0>
class BlockingQueue
{
	std::deque<T> q;
	std::condition_variable post_cv;
	std::condition_variable get_cv;
	mutable std::mutex mutex;
public:
	volatile bool unlocked = false;
	BlockingQueue() = default;
	BlockingQueue(const BlockingQueue& other) = delete;
	BlockingQueue& operator=(const BlockingQueue& other) = delete;
	BlockingQueue(BlockingQueue&& other) : q(std::move(q)) { }
	BlockingQueue& operator=(BlockingQueue&& other) { q = std::move(q); return *this; }
	void Post(T&& pkt)
	{
		std::unique_lock<std::mutex> lock(mutex);
		if (Max > 0)
		{
			post_cv.wait(lock, [&](){ return unlocked | (q.size() < Max); });
			if (q.size() >= Max) return;
		}
		q.push_back(std::forward<T>(pkt));
		get_cv.notify_one();
	}
	T Get()
	{
		static T emptyT{};
		std::unique_lock<std::mutex> lock(mutex);
		get_cv.wait(lock, [&](){ return unlocked | (q.size() > 0); });
		if (q.empty())
			return std::move(emptyT);
		T tmp = std::move(q.front());
		q.pop_front();
		if (Max > 0) post_cv.notify_all();
		return std::move(tmp);
	}
	void UnlockGetters()
	{
		unlocked = true;
		get_cv.notify_all();
		if (Max > 0) post_cv.notify_all();
	}
	int Size() const
	{
		std::lock_guard<std::mutex> lock(mutex);
		return (int)q.size();
	}
};

class IDecoder
{
	std::thread thread;
protected:
	AVFrame* frame = nullptr;
	AVStream* stream = nullptr;
	AVCodec* decoder = nullptr;
	AVCodecContext* ctx = nullptr;
	BlockingQueue<Packet> packets;
	volatile bool running = false;
	std::function<int(AVCodecContext*, AVFrame*, int*, AVPacket*)> decodeFunction;
	double minPTS = 0;

	virtual void FrameDecoded(double pts) { };
	virtual void DecodingInit() { };

	void DecodeThread()
	{
		DecodingInit();
		running = true;
		frame = av_frame_alloc();
		bool ffw = false;
		bool skipped_pkt = false;
		while (running)
		{
			auto pkt = packets.Get();

			if (!running) // still running? or Stopped, or unlocked.
				break;

			if (produceCallback)
				produceCallback();

			if (!pkt.Valid()) continue;

			auto pkt_tmp = pkt.Get();

			assert(pkt_tmp.pts != AV_NOPTS_VALUE);

			double pkt_pts = pkt_tmp.pts * av_q2d(stream->time_base);

			//if ((!pkt_tmp.flags & AV_PKT_FLAG_KEY) && !skipped_pkt && pkt_pts+.3 < minPTS) 
			//{
			//	skipped_pkt = true;
			//	continue;
			//}
			//ffw = pkt_pts < minPTS;


			int got_picture = 0;
			do {
				int ret = decodeFunction(ctx, frame, &got_picture, &pkt_tmp);
				// some codec may over-read the packet
				ret = FFMIN(ret, pkt_tmp.size);
				assert(ret >= 0);
				if (got_picture)
				{
					assert(pkt_tmp.pts != AV_NOPTS_VALUE);
					double pts = (double)av_frame_get_best_effort_timestamp(frame) * av_q2d(stream->time_base);
					//double pts = (double)pkt_tmp.pts * av_q2d(stream->time_base);

					////if (ffw && pts < minPTS) break;
					////ffw = pts < minPTS;

					//if (!ffw && pkt_pts < minPTS)
					//{
					//	ffw = true;
					//	printf("IDecoder:DecodeThread - skip packet\n");
					//	break;
					//}

					FrameDecoded(pts);
				}
				pkt_tmp.data += ret;
				pkt_tmp.size -= ret;
			} while (pkt_tmp.size > 0);
			ffw = false;
			skipped_pkt = false;
		}

		//todo: how about using refcount?
		//if (*got_frame && refcount) 
		//	av_frame_unref(frame);

		av_frame_free(&frame);
		running = false;
		printf("IDecoder::DecodeThread - Exit\n");
	}
	IDecoder(const IDecoder& other) = delete;
	IDecoder& operator=(const IDecoder& o) = delete;
public:
	// Called when a packed is consumed.
	// The owner should use it as a signal to produce.
	std::function<void()> produceCallback;

	IDecoder() = default;
	IDecoder(IDecoder&& o) :
		frame(o.frame), stream(o.stream), decoder(o.decoder), ctx(o.ctx),
		packets(std::move(packets)), running(false), decodeFunction(o.decodeFunction), minPTS(minPTS)
	{
		o.frame = nullptr;
		o.stream = nullptr;
		o.decoder = nullptr;
		o.ctx = nullptr;
		o.decodeFunction = nullptr;
		o.minPTS = 0;
	}
	IDecoder& operator=(IDecoder&& o)
	{
		IDecoder(std::forward<IDecoder>(o));
		return *this;
	}
	virtual ~IDecoder() = default;
	virtual bool Open(AVStream* av_stream)
	{
		stream = av_stream;
		ctx = stream->codec;

		if ((decoder = avcodec_find_decoder(stream->codec->codec_id)) == nullptr)
		{
			printf("IDecoder::Open \"Could not find a decoder\"\n");
			return false;
		}

		if (avcodec_open2(stream->codec, decoder, nullptr) < 0)
		{
			//printf("IDecoder::Open \"Could not open the decoder %.32s\"\n", avcodec_get_name(stream->codec->codec_id));
			auto descr = avcodec_descriptor_get(stream->codec->codec_id);
			printf("IDecoder::Open \"Could not open the decoder %.32s\"\n", descr->name);
			return false;
		}

		return true;
	}
	virtual void Start()
	{
		thread = std::thread(&IDecoder::DecodeThread, this);
	}
	virtual void Stop()
	{
		if (running)
		{
			running = false;
			packets.UnlockGetters();
			if (thread.joinable())
				thread.join();
		}
	}
	void PostPacket(AVPacket& pkt)
	{
		packets.Post(Packet(pkt));
	}
	int BufferSize() const
	{
		return packets.Size();
	}
	virtual void Close()
	{
		avcodec_close(ctx);
	}
};

class VideoDecoder : public IDecoder
{
	using super = IDecoder;

	struct VideoFrame
	{
	private:
		// Delete copying, object too fat and no need to be copied
		VideoFrame(const VideoFrame& o) = delete;
		VideoFrame& operator=(const VideoFrame&) = delete;
	public:
		bool valid = false;
		double pts = 0;
		int size = 0;
		// AVPixelFormat fmt ?? default is YUV 420
		uint8_t* data = nullptr;

		VideoFrame() = default;
		VideoFrame(double pts, uint8_t* data, int size) :
			pts(pts), size(size), data(data), valid(true) { }
		// Moveable only object
		VideoFrame(VideoFrame&& o)
		{
			*this = std::forward<VideoFrame>(o);
		}
		VideoFrame& operator=(VideoFrame&& o)
		{
			if (valid && data)
				delete data;
			pts = o.pts;
			size = o.size;
			data = o.data;
			valid = o.valid;
			o.pts = 0;
			o.size = 0;
			o.data = nullptr;
			o.valid = false;
			return *this;
		}
		~VideoFrame() { if (valid && data) delete data; }
	};

	virtual void FrameDecoded(double pts) override
	{
		int height2 = height / 2;
		int width2 = width / 2;
		int dataSize = width * (height + height2);
		uint8_t* buffer = new uint8_t[dataSize];

		// Copy Y plane
		memcpy(buffer, frame->data[0], frame->linesize[0] * height);
		// Copy U and V plane
		for (int i = 0; i < height2; i++)
		{
			memcpy(buffer + (i + height) * width, frame->data[1] + i * frame->linesize[1], width2);
			memcpy(buffer + (i + height) * width + width2, frame->data[2] + i * frame->linesize[2], width2);
		}

		q.Post(VideoFrame(pts, buffer, dataSize));
		//printf("AudioDecoder::Decode - frame decoded %f\n", pts);
	}
public:
	BlockingQueue<VideoFrame, 4> q;
	int width = 0;
	int height = 0;

	VideoDecoder(const VideoDecoder&) = delete;
	VideoDecoder(VideoDecoder&& o)
	{

	}
	VideoDecoder()
	{
		decodeFunction = avcodec_decode_video2;
	}
	//VideoDecoder(AVStream* av_stream) : VideoDecoder()
	//{
	//	assert(Open(av_stream));
	//}
	bool Open(AVStream* av_stream) override
	{
		if (!super::Open(av_stream))
			return false;

		width = ctx->width;
		height = ctx->height;

		return true;
	}
	void Stop() override
	{
		q.UnlockGetters();
		super::Stop();
	}
	VideoFrame GetFrame(double minPTS)
	{
		this->minPTS = minPTS;
		return q.Get();
		VideoFrame f;
		do { f = q.Get(); } while (running && f.pts + 3 < minPTS);
		return f;
	}
};

class AudioDecoder : public IDecoder
{
	using super = IDecoder;

	struct AudioFrame
	{
	private:
		// Delete copying, object too fat and no need to be copied
		AudioFrame(const AudioFrame& o) = delete;
		AudioFrame& operator=(const AudioFrame&) = delete;
	public:
		bool valid = false;
		double pts = 0;
		int samples = 0;
		int size = 0;
		int sampleRate = 0;
		uint8_t* data = nullptr;

		AudioFrame() = default;
		AudioFrame(int samples, double pts, int sampleRate, uint8_t* data, int size) :
			pts(pts), samples(samples), size(size), data(data), sampleRate(sampleRate), valid(true) { }
		// Movable only object
		AudioFrame(AudioFrame&& o)
		{
			*this = std::forward<AudioFrame>(o);
		}
		AudioFrame& operator=(AudioFrame&& o)
		{
			if (valid && data)
				delete data;
			pts = o.pts;
			samples = o.samples;
			size = o.size;
			data = o.data;
			valid = o.valid;
			sampleRate = o.sampleRate;
			o.pts = 0;
			o.samples = 0;
			o.size = 0;
			o.data = nullptr;
			o.valid = false;
			o.sampleRate = 0;
			return *this;
		}
		~AudioFrame() { if (valid && data) delete data; }
	};

#if defined(USE_FFMPEG)
	SwrContext* swr = nullptr;
#elif defined(USE_LIBAV)
	AVAudioResampleContext* avr = nullptr;
#endif
	BlockingQueue<AudioFrame, 8> q;

	virtual void FrameDecoded(double pts) override
	{
		int linesize;
		int dataSize = av_samples_get_buffer_size(&linesize, 2, frame->nb_samples, AV_SAMPLE_FMT_S16, 1);
		uint8_t* buffer = new uint8_t[dataSize];
#if defined(USE_FFMPEG)
		int samples = swr_convert(swr, &buffer, frame->nb_samples, (const uint8_t**)frame->data, frame->nb_samples);
#elif defined(USE_LIBAV)
		int samples = frame->nb_samples;
		avresample_convert(avr, &buffer, linesize, frame->nb_samples, frame->data, 0, frame->nb_samples);
#endif
		q.Post(AudioFrame(samples, pts, frame->sample_rate, buffer, dataSize));
		//printf("AudioDecoder::Decode - frame decoded %f\n", pts);
	}
	AudioDecoder(const AudioDecoder& o) = delete;
	AudioDecoder& operator=(const AudioDecoder& o) = delete;
public:
	AudioDecoder(AudioDecoder&& o) : super(std::forward<IDecoder>(o)), q(std::move(o.q))
	{
#if defined(USE_FFMPEG)
		if (swr) // delete context
			swr = o.swr;
		o.swr = nullptr;
#elif defined(USE_LIBAV)
		if (avr) avresample_free(&avr);
		avr = o.avr;
		o.avr = nullptr;
#endif
	}
	AudioDecoder& operator=(AudioDecoder&& o)
	{
		AudioDecoder(std::forward<AudioDecoder>(o));
		return *this;
	}
	AudioDecoder() { decodeFunction = avcodec_decode_audio4; }
	//AudioDecoder(AVStream* av_stream) : AudioDecoder()
	//{
	//	assert(Open(av_stream));
	//}
	bool Open(AVStream* av_stream) override
	{
		if (!super::Open(av_stream))
			return false;

#if defined(USE_FFMPEG)
		// we're allocating a new context
		swr = swr_alloc_set_opts(NULL,
			AV_CH_LAYOUT_STEREO,  // out_ch_layout
			AV_SAMPLE_FMT_S16,    // out_sample_fmt
			ctx->sample_rate,     // out_sample_rate
			ctx->channel_layout,  // in_ch_layout
			ctx->sample_fmt,      // in_sample_fmt
			ctx->sample_rate,     // in_sample_rate
			0,                    // log_offset
			NULL);                // log_ctx
		assert(swr != nullptr);
		auto ret = swr_init(swr);
		assert(ret >= 0);
#elif defined(USE_LIBAV)
		// Setup audio resampler
		avr = avresample_alloc_context();
		av_opt_set_int(avr, "in_channel_layout", ctx->channel_layout, 0);
		av_opt_set_int(avr, "in_sample_rate", ctx->sample_rate, 0);
		av_opt_set_int(avr, "in_sample_fmt", ctx->sample_fmt, 0);
		av_opt_set_int(avr, "out_channel_layout", AV_CH_LAYOUT_STEREO, 0);
		av_opt_set_int(avr, "out_sample_rate", ctx->sample_rate, 0);
		av_opt_set_int(avr, "out_sample_fmt", AV_SAMPLE_FMT_S16, 0);
		auto ret = avresample_open(avr);
		assert(ret == 0);
#endif

		return true;
	}
	void Stop() override
	{
		q.UnlockGetters();
		super::Stop();
	}
	AudioFrame GetFrame() { return q.Get(); }
};

class Format
{
	AVFormatContext* ctx{ nullptr };
	std::map<int, IDecoder*> streamsMap;
	std::condition_variable cv;
	std::mutex mutex;
	std::thread thread;
	volatile bool running = false;
public:
	std::vector<VideoDecoder> videoStreams;
	std::vector<AudioDecoder> audioStreams;
	//Format(const char* filename) 
	//{
	//	assert(Open(filename));
	//}
	bool Open(const char* filename)
	{
		int ret;
		if ((ret = avformat_open_input(&ctx, filename, nullptr, nullptr)) < 0)
		{
			printf("Format::Open - %s\n", error2str(ret));
			return false;
		}

		if ((ret = avformat_find_stream_info(ctx, nullptr)) < 0)
		{
			printf("Format::Open - %s\n", error2str(ret));
			avformat_close_input(&ctx);
			return false;
		}

		auto callback = [&]{ cv.notify_all(); };

		for (int i = 0; i < (int)ctx->nb_streams; i++)
		{
			switch (ctx->streams[i]->codec->codec_type)
			{
			case AVMediaType::AVMEDIA_TYPE_VIDEO:
				videoStreams.emplace_back();
				streamsMap[i] = &videoStreams.back();
				streamsMap[i]->Open(ctx->streams[i]);
				streamsMap[i]->produceCallback = callback;
				streamsMap[i]->Start();
				break;
			case AVMediaType::AVMEDIA_TYPE_AUDIO:
				audioStreams.emplace_back();
				streamsMap[i] = &audioStreams.back();
				streamsMap[i]->Open(ctx->streams[i]);
				streamsMap[i]->produceCallback = callback;
				streamsMap[i]->Start();
				break;
			}
		}

		thread = std::thread(&Format::ReaderThread, this);

		return true;
	}
	void Stop()
	{
		if (running)
		{
			running = false;
			for (auto& i : streamsMap)
				i.second->Stop();
			cv.notify_all();
			if (thread.joinable())
				thread.join();
			avformat_close_input(&ctx);
		}
	}
	void ReaderThread()
	{
		AVPacket pkt;
		running = true;
		av_init_packet(&pkt);
		while (running && av_read_frame(ctx, &pkt) >= 0)
		{
			std::unique_lock<std::mutex> lock(mutex);
			cv.wait(lock, [&]{
				using T = std::pair < int, IDecoder* > ;
				auto pred = [](const T& a, const T& b) { return a.second->BufferSize() < b.second->BufferSize(); };
				auto m = std::min_element(streamsMap.begin(), streamsMap.end(), pred)->second;
				return !running | (m->BufferSize() < 5);
			});
			if (!running) break;
			auto stream = streamsMap.find(pkt.stream_index);
			if (stream != streamsMap.end())
				stream->second->PostPacket(pkt);
			else
				av_free_packet(&pkt);
			av_init_packet(&pkt);
		}
		if (!running)
			avformat_close_input(&ctx);
		printf("Format::ReaderThread - EOF\n");
	}
};
